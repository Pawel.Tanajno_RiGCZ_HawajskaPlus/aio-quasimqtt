# aio-QuasiMQTT

**aio-QuasiMQTT** is a project, which aims in simulating MQTT protocol in an asynchronous (input/output) manner. The main goal is to handle both TCP and UDP connections from many clients at the one time. The basic authorization has been implemented.


#### Requirements

1. Python 3.7+
2. [janus](https://pypi.org/project/janus/) third-party library, implements queues which can be accessed from synchronous and asynchronous code.
3. [pyowm](https://pyowm.readthedocs.io/en/latest/) third-party library, it allows us to get current weather which we use in one of the implemented channels.


#### Before you start

1. More info about [MQTT](https://en.wikipedia.org/wiki/MQTT).
2. More info about [asyncio](https://docs.python.org/3/library/asyncio.html).



## How it works?

Either server and client programs have been written using asynchronous syntax of Python. On the lower level, they use asyncio's [Transports and Protocols](https://docs.python.org/3/library/asyncio-protocol.html), where they were used to define TCP server / connection and UDP endpoints parameters. Also these programs implement callbacks for possible transport layer events like opening connection or receiving data.



### Starting program

#### Server's side

Server's parameters are specified in `quasimqqt/config/config.py`. When server starts the basic setup is performed (creating TCP server, opening UDP endpoint). If everything is as expected you should get these messages in console:

```
server-manager: INFO - Server setup has been finished!
server-manager: INFO - [UDP] ready
server-manager: INFO - [TCP] ready
```

Now server is listening both on TCP and UDP ports, waiting for connections from clients.

#### Client's side

Client's startup is very similar. When running client's program you should specify: **IPv4** address in decimal form and both TCP and UDP ports (even if some isn't used). Type `--help` while running for more details. After client's setup, the connection will be attempted. Exemplary logging:

```
client-manager: INFO - Client setup has been finished
client-manager: INFO - [TCP] trying to connect with server...
client-manager: INFO - [UDP] trying to connect with server...
client-manager: INFO - [UDP] ready
client-manager: INFO - [UDP] trying to connect with QuasiMQTT app...
client-manager: DEBUG - [UDP] data sent: {"type": "ctrl", "client_id": 4284279186, "event": "connection-request"}
client-tcp: INFO - Successfully connected with server: ('127.0.0.1', 8888)
client-manager: INFO - [TCP] ready
client-manager: INFO - [TCP] trying to connect with QuasiMQTT app...
client-manager: DEBUG - [TCP] data sent: {"type": "ctrl", "client_id": 4284279186, "event": "connection-request"}
client-tcp: DEBUG - Received data {'type': 'ctrl', 'event': 'connection-accepted'} from server
client-manager: INFO - [TCP] successfully connected to server application
client-manager: INFO - [UDP] trying to connect with QuasiMQTT app...
client-manager: DEBUG - [UDP] data sent: {"type": "ctrl", "client_id": 4284279186, "event": "connection-request"}
client-udp: DEBUG - Received data {'type': 'ctrl', 'event': 'connection-accepted'} from server
client-manager: INFO - [UDP] successfully connected to server application
```

Connection with broker is done in steps:

1. Connect with TCP.
2. Register TCP endpoint in server application.
3. Register UDP endpoint in server application.

For 2 i 3 you can apply any policy of registering client in broker. By default, every client with a different client identifier is registered.



**Registration on server's side**

```markdown
server-manager: INFO - Server setup has been finished!
server-manager: INFO - [UDP] ready
server-manager: INFO - [TCP] ready
server-manager: DEBUG - [UDP] checking for peers reachability...
server-manager: DEBUG - [UDP] checking for peers reachability...
server-tcp: INFO - Connection from ('127.0.0.1', 5002)
server-manager: INFO - [TCP] peer ('127.0.0.1', 5002) transport connected
server-udp: DEBUG - Received data {'type': 'ctrl', 'client_id': 4284279186, 'event': 'connection-request'} from ('127.0.0.1', 5001)
server-manager: INFO - [UDP] peer ('127.0.0.1', 5001) transport connected
**server-manager: INFO - [UDP] peer ('127.0.0.1', 5001) with ID 4284279186 registered**
server-manager: WARNING - [UDP] data {'type': 'ctrl', 'event': 'connection-accepted'} sent to ('127.0.0.1', 5001)
server-tcp: DEBUG - Received data {'type': 'ctrl', 'client_id': 4284279186, 'event': 'connection-request'} from ('127.0.0.1', 5002)
**server-manager: INFO - [TCP] peer ('127.0.0.1', 5002) with ID 4284279186 registered**
server-manager: DEBUG - [TCP] Sending data {'type': 'ctrl', 'event': 'connection-accepted'} to 4284279186 (('127.0.0.1', 5002))
server-manager: DEBUG - [TCP] data {'type': 'ctrl', 'event': 'connection-accepted'} sent to ('127.0.0.1', 5002)
```





### Flow

Client application communicates with server application through transport layer and connection manager (the same reversely). Both Client Connection Manager and Server Connection Managers stores necessary information to establish and maintain communication, making it more robust.



```mermaid
graph LR
	C[Client Connection Manager] ---|UDP <br>Requirements|U[UDP <br>Transmission] ---|UDP <br>Requirements| S[Server Connection Manager]
	C[Client Connection Manager] ---|TCP <br>Requirements|T[TCP <br>Transmission] ---|TCP <br>Requirements| S[Server Connection Manager]
	
	CA[Client App] --- C
	SA[Server App] --- S
	
```



All exchanges of messages between clients have to pass-through the server.

```mermaid
graph LR
	C1[Client 1] --- S[Server]
	C2[Client 2] --- S[Server]
	C3[Client 3] --- S[Server]
```

### Server Connection Manager

Basically, it interconnects the transport and the application layers. It creates a TCP server, opens UDP endpoint, manages reading, and sending data to proper destinations. 

#### Main features

1. Setup server.
2. Opening and closing TCP server.
3. Opening and closing UDP endpoint.
4. Reading TCP and UDP data, handling control data (like application connection requests).
5. Registering clients, resolving their identifiers to pair (ip address, port number) either for TCP and UDP.
6. Implementation of keep-alive for UDP.
7. Holding client's connection state information.
8. Notifying application layer that some data managed to send couldn't be sent.
9. Caching UDP data managed to send for specific UDP peer during client dead interval (till time keep-alive mechanism spot client is unreachable).
10. Graceful shutdown (with notification of UDP clients that server's endpoint will be closed). 

### The server application

The main tasks of the application layer:
1. Handling customer requests
2. Receiving information on the client's current connection status from lower levels, and taking appropriate actions related to it
3. Remembering messages addressed to the client that did not request to disconnect and with which the server lost connection. Send these messages after reconnection.
4. Authentication of clients based on their id and password.

To meet the above requirements, the server application uses three databases. The sqlalchemy library was used to manage the databases.
More about [sqlalchemy] (https://docs.sqlalchemy.org/en/13/)

Description of databases
1. user.db - a database containing an authentication table containing entries of all users and a Topic table containing data on the channels subscribed by individual clients.
The main task of this database is to authenticate the user and check what channels a particular user has subscribed to.
  - Authentication table consist of
      peer_id (integer)
      client_id (text)
      password (text)
      last_will_and_testament (DateTime)

  - Topic table, which assigns users to channels (topics), consist of: 
      client_id (text)
      topic_name t (text)
      keep_updated (bool)
      last_activity (DateTime)

2. topic.db - storing data about channels (topics)

  - Topics table consist of:
      organiser (text)
      topic_name (text)
      transport_layer_protocol (text)

3. message.db - database containing channel tables to which at least one user with the KU flag turned on is connected (more in the section "Communication between client and server application layers".  

  - KeepUpdate table - Contains a list of clients to send messages to after reconnecting with them, consist of:
      peer_id (integer)
      client_id (text)
      topic_name (text)
      is_connected (bolean)
      timestamp (DateTime)

  - Messages table stores messages that did not reach the clients registered in the KeepUpdate table, consist of:
      topic_name (text)
      sender_id (text)
      content (text)
      timestamp (DateTime) 
      transport_layer_protocol (text)

### Client Connection Manager

Equivalent for client.

#### Main features

1. Setup client.
2. Establishing (**with retry**) and closing connection with TCP server.
3. Opening and closing UDP endpoint.
4. Connecting with application layer.
5. Reading TCP and UDP data, handling control data (like application connection acceptations).
6. Implementation of keep-alive for UDP.
7. When server is unreachable, application layer is notified.
8. Caching UDP data managed during server dead interval (till time keep-alive mechanism spot server is unreachable).
9. Graceful shutdown (with notification of UDP server that client's endpoint will be closed). 


### Client config
In quasimqtt/config/ there are files with .json extension, e.g. client1.json
You choose one after -f parameter while launching client.py and it determines client application behaviour.
You can choose which channels you want to establish, subscribe, or publish on.
Remember, that you have to subscribe channels, that you want to publish on.
Structure of this file is as follows:

```json
{
	"id": "your id, choose yourself, string",
	"password": "your password, choose yourself, int",
	"last will": "this message will be send to everyone, when you will lose connection with server, string",
	"channels to establish": {
		"channel name": {
			"transport protocol": "TCP or UDP, string"
		}
	},
	"channels to publish": {
		"channel name": { "period": "number of seconds between sended messages, int" }
	},
	"channels to subscribe": {
		"channel name": {
			"last will testament": "yes or no, it will determine if you will send your last will on this channel, string",
			"keep updated": "yes or no, it will determine if outstanding messages, e.g. after you lost connection, will be sent to you or not, string"
		}
	}
}
```

You can add more than one channel to establish, publish or subscribe. Just separate them by comma.
Examples are in client1.json and client2.json


In the current version of the configuration files, three channels are created:
1. "outside_temperature" which sends current temperature in Krakow
2. "random_number" which sends random number between 100 and 1000
3. "time_stamp" which sends current date and time

You should launch client (same as server) from aio-quasimqtt folder, so all libraries and paths will work properly.
While launching client.py, you should pass name of config file without extension, i.e. -f client1

### Server launch preview:
![obraz_1.png](./obraz_1.png)

### Client1 launch preview:
![obraz_4.png](./obraz_4.png)

### Client2 launch preview:
![obraz_3.png](./obraz_3.png)


### Communication between the client and server application layers

The solution used:
Messages between server and client applications are sent in the form of JSON objects.

Description of the keys used
1. msg_type - message type
2. tp - transport layer protocol identifier;
3. id - client id in the application layer (stored in databases as client_id);
4. pwd - password;
5. lwt - last will and testament flag, the value will be the last message of the client that disconnected;
6. ku -keep updated - a flag informing the server if the client wants messages to be sent to it after reconnection, which came to the server after its accidental disconnection;
7. top - The topic name,
8. sr - subscription response - server response to the subscription request;
9. ches - channel establishment - server response to the request to establish a new channel;
10. pr - publication response - server response to publication request;
11. ud - user data;
12. s - sender's id

Types of messages sent between applications
1. Connection request (msg_type = 0) - the first message that the client sends to the server.
2. Connection confirmation (msg_type = 1) - the server responds to the client if the authentication was successful.
3. Channel subscription request (msg_type = 2).
4. Confirmation of subscription receipt (msg_type = 3) - the server informs the client if the subscription success.
5. Subscription termination request (msg_type = 4) - client sends to server.
6. Confirmation of unsubscription (msg_type = 5) - the server sends it to the client.
7. Publish request (msg_type = 6) - the client sends the message that he wants to distribute on the given channel.
8. Publishing confirmation (msg_type = 7) -server sends information about publication success.
9. Request to establish a new channel (msg_type = 8).
10. Confirmation of establishing a new channel (msg_type = 9) - the server sends information about the success of establishing a new channel.
11. Disconnect request (msg_type = 10) - the client sends a disconnect request to the server application layer.
12. Disconnection confirmation (msg_type = 11).
13. Message sent to subscribers (msg_type = 12) - message sent from the server to users subscribing to the channel, except for the sender.
