import asyncio
import logging
import json
import janus
import argparse
import time
import random
import multiprocessing

from quasimqtt.protocols.prototcp import TCPClientProtocol
from quasimqtt.protocols.protoudp import UDPClientProtocol
from quasimqtt.config.config import *
from quasimqtt.client.clientapp import ClientApplication


logging.basicConfig(format="%(name)s: %(levelname)s - %(message)s") #, level=logging.INFO)


class ClientConnectionManager:
    def __init__(self, loop, server_config, client_config, cli_app, client_identifier):
        self.loop = loop

        self.data_q = None
        self.server_config = server_config
        self.client_config = client_config
        self.clients = {
            "TCP": TCPClientProtocol(),
            "UDP": UDPClientProtocol()
        }
        self.clients["TCP"].logger.setLevel(logging.ERROR)
        self.clients["UDP"].logger.setLevel(logging.ERROR)
        self.last_seen = {}
        self.app_connections = {
            "TCP": False,
            "UDP": False
        }
        self.udp_cache = []

        self.client_application = cli_app

        self.logger = logging.getLogger("client-manager")
        self.logger.setLevel(logging.INFO)

        #self.client_id = random.randint(int(1e9), int(1e10-1))
        self.client_id = client_identifier

        self.keep_alive_udp_task = None
        self.watch_udp_server_unreachable_task = None
        self.pull_data_task = None
        self.connect_with_server_app_tcp_task = None
        self.connect_with_server_app_udp_task = None

    async def client_setup(self):
        self.data_q = janus.Queue()
        self.clients["TCP"].data_q = self.data_q
        self.clients["UDP"].data_q = self.data_q

        self.loop.create_task(self.try_connect("TCP"))
        self.loop.create_task(self.try_connect("UDP"))
        self.loop.create_task(self.read_data())
        self.pull_data_task = self.loop.create_task(self.pull_data())
        self.watch_udp_server_unreachable_task = self.loop.create_task(self.watch_udp_server_unreachable())
        self.loop.create_task(self.client_application.watch_dog())
        self.keep_alive_udp_task = self.loop.create_task(self.keepalive_udp())

        self.client_application.data_to_send_q = janus.Queue()
        self.client_application.transport_app_communication = janus.Queue()
        self.logger.info("Client setup has been finished")

    async def shutdown(self):
        await self.pass_data_to_app("quitting")
        await asyncio.sleep(0.1)
        self.logger.info("Disconnection with server application completed")
        await self.data_q.async_q.put((None, None, True))
        self.pull_data_task.cancel()
        self.keep_alive_udp_task.cancel()
        self.watch_udp_server_unreachable_task.cancel()
        await self._send({"type": "ctrl", "client_id": self.client_id, "event": "leaving"}, "UDP")
        await asyncio.sleep(2)

    # RUN SECTION
    async def _connect_with_server_app(self, protocol):
        while True:
            self.logger.info(f"[{protocol}] trying to connect with QuasiMQTT app...")
            await self._send({"type": "ctrl", "client_id": self.client_id, "event": "connection-request"}, protocol)
            await asyncio.sleep(5)

    async def _connect_tcp(self):
        transport, protocol = await self.loop.create_connection(
            protocol_factory=self.clients["TCP"],
            host=self.server_config["TCP"]["IPV4"],
            port=self.server_config["TCP"]["PORT"],
            local_addr=(self.client_config["TCP"]["IPV4"], self.client_config["TCP"]["PORT"])
        )
        return transport

    async def _connect_udp(self):
        transport, protocol = await self.loop.create_datagram_endpoint(
            protocol_factory=self.clients["UDP"],
            remote_addr=(self.server_config["UDP"]["IPV4"], self.server_config["UDP"]["PORT"]),
            local_addr=(self.client_config["UDP"]["IPV4"], self.client_config["UDP"]["PORT"])
        )
        return transport

    async def _connect(self, protocol):
        connect = self._connect_tcp if protocol == "TCP" else self._connect_udp
        try:
            transport = await connect()
            return transport
        except Exception as e:
            self.logger.warning(f"Cannot establish [{protocol}] connection with server, reason:\n{str(e)}")
            return None

    async def connect(self, protocol):
        transport = await self._connect(protocol)
        if transport:
            self.last_seen[protocol] = time.time()
            self.clients[protocol].is_closed = self.loop.create_future()
            self.logger.info(f"[{protocol}] ready")
            if protocol == "TCP":
                self.connect_with_server_app_tcp_task = self.loop.create_task(self._connect_with_server_app(protocol))
            else:
                self.connect_with_server_app_udp_task = self.loop.create_task(self._connect_with_server_app(protocol))
            try:
                await self.clients[protocol].is_closed
                await self.client_application.transport_app_communication.async_q.put((protocol, "disconnected"))
                self.app_connections[protocol] = False
            finally:
                self.clients[protocol].transport.close()
                self.logger.info(f"[{protocol}] closed")
        else:
            self.clients[protocol].transport = None

    async def try_connect(self, protocol):
        while True:
            if self.clients[protocol].transport is None or self.clients[protocol].transport.is_closing():
                self.logger.info(f"[{protocol}] trying to connect with server...")
                await self.connect(protocol)
            await asyncio.sleep(5)

    # SENDING SECTION

    async def _send(self, data_to_send, protocol):
        try:
            data = json.dumps(data_to_send) + ","
            if protocol == "TCP":
                self.clients["TCP"].transport.write(data.encode())
            else:
                self.clients["UDP"].transport.sendto(
                    data.encode(),
                    (SERVER_CONFIG["UDP"]["IPV4"], SERVER_CONFIG["UDP"]["PORT"])
                )
            self.logger.debug(f"[{protocol}] data sent: {data}")
        except Exception as e:
            self.logger.error(f"Troubles with sending data\n{str(e)}")

    async def send_data(self, data_to_send: dict):
        tcp_data, udp_data = data_to_send["TCP"], data_to_send["UDP"]
        if tcp_data:
            data = {"type": "data", "client_id": self.client_id, "payload": tcp_data}
            await self._send(data, "TCP")
        if udp_data:
            self.udp_cache.extend(udp_data)
            data = {"type": "data", "client_id": self.client_id, "payload": udp_data}
            await self._send(data, "UDP")

    # HANDLING NEW DATA SECTION

    async def _handle_control_data(self, protocol, data):
        if data["event"] == "keepalive":
            await self._send({"type": "ctrl", "event": "keepalive"}, protocol)
        elif data["event"] == "leaving":
            await self.client_application.transport_app_communication.async_q.put((protocol, "disconnected"))
            self.last_seen.pop("UDP")
            self.app_connections[protocol] = False
        elif data["event"] == "connection-accepted" and not self.app_connections[protocol]:
            self.logger.info(f"[{protocol}] successfully connected to server application")
            if protocol == "TCP":
                self.connect_with_server_app_tcp_task.cancel()
            else:
                self.connect_with_server_app_udp_task.cancel()
            self.app_connections[protocol] = True
            await self.client_application.transport_app_communication.async_q.put((protocol, "connected"))

    async def read_data(self):
        while True:
            try:
                protocol, data, bk = await self.data_q.async_q.get()
                if bk:
                    break
                if protocol == "UDP":
                    self.udp_cache.clear()
                    await self.check_udp_server_status()
                self.register_time(protocol)
                if data["type"] == "ctrl":
                    await self._handle_control_data(protocol, data)
                else:
                    await self.pass_data_to_app(data)
            except Exception as e:
                if str(e) != "":
                    self.logger.error(f"Messages can't be read, reason:\n{str(e)}")
    # CONNECTION MAINTENANCE SECTION

    async def watch_udp_server_unreachable(self):
        while True:
            await asyncio.sleep(3)
            if "UDP" in self.last_seen:
                self.logger.debug("[UDP] checking for server reachability...")
                last_seen = time.time() - self.last_seen["UDP"]
                self.logger.debug(f"[UDP] server last seen {last_seen} seconds ago")
                if last_seen > 10:
                    self.logger.info("[UDP] server unreachable")
                    await self.client_application.transport_app_communication.async_q.put(("UDP", "disconnected"))
                    self.last_seen.pop("UDP")
                    self.app_connections["UDP"] = False
                    await self.client_application.data_not_sent("UDP", self.udp_cache.copy())
                    self.udp_cache.clear()

    async def keepalive_udp(self):
        while True:
            await asyncio.sleep(5)
            self.logger.debug(f"[UDP] sending keepalive")
            await self._send({"type": "ctrl", "client_id": self.client_id, "event": "keepalive-client"}, "UDP")

    async def check_udp_server_status(self):
        if "UDP" not in self.last_seen:
            self.logger.info("[UDP] server is reachable now")
            self.connect_with_server_app_udp_task = self.loop.create_task(self._connect_with_server_app("UDP"))

    def register_time(self, protocol):
        self.last_seen[protocol] = time.time()

    # INTERACTION WITH APP SECTION

    async def pass_data_to_app(self, data):
        await self.client_application.consume_data(data)

    async def pull_data(self):
        while True:
            data = await self.client_application.data_to_send_q.async_q.get()
            await self.send_data(data)


parser = argparse.ArgumentParser(description='Client side in QuasiMQQT application')

parser.add_argument('-a', '--ip_addr', type=str, metavar='', required=True, help="Client's IPv4 address in decimal "
                                                                                 "format, i.e: 192.168.0.1")
parser.add_argument('-t', '--tcp_port', type=int, metavar='', required=True, help="Client's TCP port")
parser.add_argument('-u', '--udp_port', type=int, metavar='', required=True, help="Client's UDP port")
parser.add_argument('-f', '--file_name', type=str, metavar='', required=True, help="Filename of client app config ("
                                                                                   "without extension), "
                                                                                   "i.e. clientapp_config1")
parser.add_argument('-i', '--identifier', type=int, required=True, help="Client's application identifier")

args = parser.parse_args()

CLIENT_TCP = {
    "IPV4": args.ip_addr,
    "PORT": args.tcp_port
}

CLIENT_UDP = {
    "IPV4": args.ip_addr,
    "PORT": args.udp_port
}
CLIENT_CONFIG = {
    "TCP": CLIENT_TCP,
    "UDP": CLIENT_UDP
}


def parsing_clientapp_config():
    path = 'quasimqtt/config/' + args.file_name + '.json'
    with open(path, 'r') as client_config_json:
        client_config = json.load(client_config_json)
        return client_config


async def shutdown():
    tasks = [task for task in asyncio.all_tasks() if task is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    await asyncio.gather(*tasks)


def main():
    loop = asyncio.get_event_loop()

    client_config = parsing_clientapp_config()
    application = ClientApplication(loop, client_config)
    ccm = ClientConnectionManager(loop, SERVER_CONFIG, CLIENT_CONFIG, application, args.identifier)


    try:
        loop.create_task(ccm.client_setup())
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        try:
            loop.run_until_complete(ccm.shutdown())
            loop.run_until_complete(shutdown())
        except asyncio.CancelledError:
            pass
        loop.close()


main()
