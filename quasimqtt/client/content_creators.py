import pyowm
from datetime import datetime
from random import randint


def time_stamp():
    now = datetime.now()
    message = now.strftime("%d/%m/%Y %H:%M:%S")
    return message


def outside_temperature():
    owm = pyowm.OWM('8490dd7c6b24aa4d911827e7dc67b380')
    manager = owm.weather_manager()
    observation = manager.weather_at_place('Krakow, PL')
    weather = observation.weather.temperature('celsius')
    return weather


def random_number():
    return randint(100, 1000)
