import asyncio
import copy
import logging
import time
from quasimqtt.client.content_creators import *

logging.basicConfig(format="%(name)s: %(levelname)s - %(message)s", level=logging.INFO)


class ClientApplication:
    def __init__(self, loop, config):
        self.loop = loop
        self.produce_data_tcp_task = None
        self.produce_data_udp_task = None

        self.data_to_send_q = None

        self._application_data = {
            "TCP": [],
            "UDP": []
        }

        self.transport_app_communication = None
        self.logger = logging.getLogger("client-app")
        self.logger.setLevel(logging.DEBUG)
        if 'channels to establish' in config:
            self.channels_to_establish = config['channels to establish']
        if 'channels to publish' in config:
            self.channels_to_publish = config['channels to publish']
        if 'channels to subscribe' in config:
            self.channels_to_subscribe = config['channels to subscribe']
        self.app_id = config['id']
        self.password = config['password']
        self.last_will = config['last will']
        self.known_channels = {}
        self.connected = False

    # don't touch
    async def watch_dog(self):
        while True:
            protocol, event = await self.transport_app_communication.async_q.get()
            if event == "disconnected":
                try:
                    if protocol == "TCP":
                        # pass
                        self.produce_data_tcp_task.cancel()
                    else:
                        self.produce_data_udp_task.cancel()
                    self.logger.info(f"cancelled producing new data for {protocol}")
                except AttributeError:  # think about preventing this exception
                    pass
            elif event == "connected":
                if protocol == "TCP":
                    # pass
                    self.produce_data_tcp_task = self.loop.create_task(self.produce_data_tcp())
                else:
                    self.produce_data_udp_task = self.loop.create_task(self.produce_data_udp())
                self.logger.info(f"started with producing data for {protocol}")

    async def data_not_sent(self, protocol, payload):
        """
        Handles data which was managed to send but wasn't.
        :param protocol: a protocol though which the data was managed to send.
        :param payload: actual data.
        :return:
        """
        self.logger.info(f"Handling data {payload} which was not sent over {protocol}...")
        return

    async def produce_data_tcp(self):
        if not self.connected:
            self.connect_to_application()
        await self.push_data()
        for channel, parameters in self.channels_to_establish.items():
            self.establish_new_channel(channel, parameters)
        await self.push_data()
        await asyncio.sleep(0.5)
        for channel, parameters in self.channels_to_subscribe.items():
            self.subscribe_channel(channel, parameters)
        await self.push_data()
        if not self.channels_to_publish:
            return
        while True:
            for channel, parameters in self.channels_to_publish.items():
                if channel in self.known_channels and self.known_channels[channel]['transport protocol'] == 'TCP':
                    await asyncio.sleep(0.1)
                    now = time.time()
                    try:
                        if parameters['period'] > now - self.channels_to_publish[channel]['previous sending time']:
                            continue
                        else:
                            message = globals()[channel]()
                            self.publish_a_message(channel, message)
                            self.channels_to_publish[channel]['previous sending time'] = now
                            await self.push_data()
                    except KeyError:
                        self.channels_to_publish[channel]['previous sending time'] = 0

    async def produce_data_udp(self):
        if not self.connected:
            self.connect_to_application()
        while True:
            await asyncio.sleep(0.1)
            for channel, parameters in self.channels_to_publish.items():
                if channel in self.known_channels and self.known_channels[channel]['transport protocol'] == 'UDP':
                    await asyncio.sleep(0.1)
                    now = time.time()
                    try:
                        if parameters['period'] > now - self.channels_to_publish[channel]['previous sending time']:
                            continue
                        else:
                            message = globals()[channel]()
                            self.publish_a_message(channel, message)
                            self.channels_to_publish[channel]['previous sending time'] = now
                            await self.push_data()
                    except KeyError:
                        self.channels_to_publish[channel]['previous sending time'] = 0

    async def consume_data(self, data):
        """
        Here we can get data sent by server.
        :param data: Data send by server
        """
        if isinstance(data, str) and data == 'quitting':
            self.disconnect()
            await self.push_data()
            return
        payload = data['payload']
        for message in payload:
            if message['id'] != self.app_id:
                continue
            message_types = {0b0001: "connection_confirmation", 0b0011: "subscription_confirmation",
                             0b0101: "subscription_resignation_confirmation", 0b0111: "publication_confirmation",
                             0b1001: "channel_establishment_confirmation", 0b1011: "disconnect_confirmation",
                             0b1100: "message_receiver"}
            bin_message_type = message['msg_type']
            getattr(self, message_types[bin_message_type])(message)
        return

    async def push_data(self, data=None):
        if data is None:
            data = copy.deepcopy(self._application_data)
            self._application_data["TCP"].clear()
            self._application_data["UDP"].clear()
        await self.data_to_send_q.async_q.put(data)

    def connect_to_application(self):
        bin_message_type = 0b0000
        tp = 'TCP'
        if self.last_will == '':
            l = 0b0
        else:
            l = self.last_will
        self._application_data[tp].append(
            {'msg_type': bin_message_type, 'id': self.app_id, 'tp': tp, 'pwd': self.password, 'l': l})
        self.connected = True

    def subscribe_channel(self, channel, parameters):
        bin_message_type = 0b0010
        tp = "TCP"
        ku = 0b1 if self.channels_to_subscribe[channel]['keep updated'] == 'yes' else 0b0
        if self.channels_to_subscribe[channel]['last will testament'] == 'yes':
            l = self.last_will
        else:
            l = 0b0
        self.channels_to_subscribe[channel]['transport protocol'] = tp
        self._application_data[tp].append(
            {'msg_type': bin_message_type, 'id': self.app_id, 'tp': tp, 'pwd': self.password, 'ku': ku, 'l': l,
             'top': channel})

    def unsubscribe_channel(self, parameters):
        bin_message_type = 0b0100
        channel_name = input('Please, insert channel name you would like to unsubscribe: ')
        try:
            tp = self.known_channels[channel_name]['transport protocol']
        except KeyError:
            print('Incorrect channel name')
            return
        self._application_data[tp].append(
            {'msg_type': bin_message_type, 'id': self.app_id, 'tp': tp, 'pwd': self.password})

    def publish_a_message(self, channel, message):
        bin_message_type = 0b0110
        try:
            tp = self.known_channels[channel]['transport protocol']
        except KeyError:
            print('Incorrect channel name')
            return
        ud = message
        self._application_data[tp].append(
            {'msg_type': bin_message_type, 'id': self.app_id, 'top': channel, 'tp': tp, 'pwd': self.password, 'ud': ud})

    def establish_new_channel(self, channel, parameters):
        if channel not in self.channels_to_subscribe:
            print("You have to subscribe channels that you establish")
            exit()
        bin_message_type = 0b1000
        tp = parameters['transport protocol']
        if 'keep updated' in self.channels_to_subscribe[channel]:
            ku = 0b1 if self.channels_to_subscribe[channel]['keep updated'] == 'yes' else 0b0
        else:
            ku = 0b0
        self.known_channels[channel] = {}
        self.known_channels[channel]['transport protocol'] = tp
        self._application_data[tp].append(
            {'msg_type': bin_message_type, 'id': self.app_id, 'tp': tp, 'pwd': self.password, 'top': channel, 'ku': ku})

    def disconnect(self):
        bin_message_type = 0b1010
        tp = 'TCP'
        self._application_data[tp].append(
            {'msg_type': bin_message_type, 'id': self.app_id, 'tp': tp, 'pwd': self.password})

    def connection_confirmation(self, message):
        if message['id'] != 0:
            self.logger.info(f"You have successfully connected to server application. Your ID is now {message['id']}")
        else:
            self.logger.error(f"Your ID is used already, you did not establish connection to server application. ")

    def subscription_confirmation(self, message):
        if message['sr'] == 0b0 and 'top' in message:
            self.logger.info(f"You have successfully subscribed channel {message['top']}")
            if message['top'] not in self.known_channels:
                self.known_channels[message['top']] = {}
            self.known_channels[message['top']]['transport protocol'] = message['tp']
            self.known_channels[message['top']]['subscribe'] = True
        elif message['sr'] == 0b1:
            self.logger.error(f"There is not such as channel as {message['top']}")
        elif message['sr'] == 0b10:
            self.logger.error("Your ID is not recognized in servers database")
        elif message['sr'] == 0b11:
            self.logger.error("Authentication error")

    def subscription_resignation_confirmation(self, message):
        if message['sr'] == 0b0 and 'top' in message:
            self.logger.info(f"You have successfully unsubscribed channel {message['top']}")
            self.known_channels[message['top']]['subscribe'] = False
            if message['top'] == 0b1:  # ??? xD
                self.known_channels.pop('tcp', None)
            else:
                self.known_channels.pop('udp', None)
        elif message['sr'] == 0b10:
            self.logger.error("Your ID is not recognized in servers database")
        elif message['sr'] == 0b11:
            self.logger.error("Wrong password")

    def publication_confirmation(self, message):
        if message['pr'] == 0b0:
            self.logger.debug(f"You have successfully published your message in {message['top']}")
        elif message['pr'] == 0b1:
            self.logger.error(f"There is not such as channel as {message['top']}")
        elif message['pr'] == 0b10:
            self.logger.error("Your ID is not recognized in servers database")

    def channel_establishment_confirmation(self, message):
        if message['ches'] == 0b0:
            self.logger.info(f"You have successfully established new channel {message['top']}")
        elif message['ches'] == 0b1:
            self.logger.error(f"There is already channel named {message['top']}")
        elif message['ches'] == 0b10:
            self.logger.error("Your ID is not recognized in servers database")
        elif message['sr'] == 0b11:
            self.logger.error("Wrong password")

    def message_receiver(self, message):
        print(f'\nMessage from {message["s"]} on {message["top"]} channel: ', end='')
        try:
            user_data = eval(message['ud'])
        except NameError:
            user_data = message['ud']
        except SyntaxError:
            user_data = message['ud']
        if isinstance(user_data, dict):
            print('')
            for key in user_data:
                print(key, ': ',  user_data[key])
        else:
            print(message["ud"])

    def disconnect_confirmation(self, message):
        self.logger.info("You have successfully disconnect from server application")
        self.known_channels.clear()
