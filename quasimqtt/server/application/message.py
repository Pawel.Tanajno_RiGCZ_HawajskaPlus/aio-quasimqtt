
class Message:

    def __init__(self, sender_id, table_name, content, timestamp):
        self.sender_id = sender_id
        self.table_name = table_name
        self.content = content
        self.timestamp = timestamp
