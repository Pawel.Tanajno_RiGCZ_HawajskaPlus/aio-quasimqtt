import os
from sqlalchemy import Column, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool
from sqlalchemy.exc import IntegrityError

BaseModel = declarative_base()


class Topic(BaseModel):
    __tablename__ = 'topics_table'
    organiser = Column(String, primary_key=True, nullable=False)
    topic_name = Column(String(), primary_key=True, nullable=False)
    transport_layer_protocol = Column(String(), primary_key=True, nullable=False)


class TopicDatabaseManager:
    def __init__(self):
        self.message_database_name = 'topic.db'
        if os.path.exists(self.message_database_name):
            os.remove(self.message_database_name)
        self.message_database = create_engine('sqlite:///' + self.message_database_name, poolclass=NullPool)

        BaseModel.metadata.create_all(self.message_database)
        self.db_session = sessionmaker(bind=self.message_database)
        self.session = self.db_session()

    def __del__(self):
        Topic.__table__.drop(self.message_database)
        self.session.commit()
        self.session.close()

    def exist_topic(self, topic_name):
        self.session = self.db_session()
        exist = self.session.query(Topic).filter(Topic.topic_name == topic_name).scalar()
        self.session.commit()
        if exist is None:
            return False
        else:
            return True

    def add_topic(self, organiser, topic_name, transport_layer_protocol):
        self.session = self.db_session()
        try:
            self.session.add_all([
                Topic(organiser=organiser, topic_name=topic_name,
                      transport_layer_protocol=transport_layer_protocol, ), ])
            self.session.commit()
        except IntegrityError:
            pass

    def topics_organiser(self, client_id, topic_name):
        self.session = self.db_session()
        exist = self.session.query(Topic).filter(Topic.topic_name == topic_name).filter(
            Topic.organiser == client_id).scalar()
        self.session.commit()
        if exist is None:
            return False
        else:
            return True

    def remove_topic(self, topic_name):
        self.session = self.db_session()
        self.session.query(Topic).filter(Topic.topic_name == topic_name).delete()
        self.session.commit()

    def change_organiser(self, client_id, topic_name):
        self.session = self.db_session()
        inst_activity = self.session.query(Topic).filter(Topic.topic_name == topic_name).scalar()
        try:
            inst_activity.organiser = client_id
        except AttributeError:
            print("update activity - record not found")
        self.session.commit()


'''
topic_database_manager = TopicDatabaseManager()
print(topic_database_manager.exist_topic("temperature"))
topic_database_manager.add_topic("Karol", "temperature", "TCP")
print(topic_database_manager.exist_topic("temperature"))
print(topic_database_manager.topics_organiser("Michał", "temperature"))
print(topic_database_manager.topics_organiser("Michał", "humidity"))
print(topic_database_manager.topics_organiser("Karol", "temperature"))
print(topic_database_manager.topics_organiser("Karol", "humidity"))
topic_database_manager.change_organiser("Michał", "temperature")
print("-----------------")
print(topic_database_manager.topics_organiser("Michał", "temperature"))
print(topic_database_manager.topics_organiser("Michał", "humidity"))
print(topic_database_manager.topics_organiser("Karol", "temperature"))
print(topic_database_manager.topics_organiser("Karol", "humidity"))
print("-----------------")
topic_database_manager.remove_topic("temperature")
print(topic_database_manager.exist_topic("temperature"))
'''
