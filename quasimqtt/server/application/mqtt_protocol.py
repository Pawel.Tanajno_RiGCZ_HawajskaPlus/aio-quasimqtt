from quasimqtt.server.application.json_parser import JsonParser
from quasimqtt.server.application.user_database_manager import UserDatabaseManager
from quasimqtt.server.application.message_database_manager import MessageDatabaseManager
from quasimqtt.server.application.topic_database_manager import TopicDatabaseManager
from datetime import datetime
import copy


class MqttApplication:

    def __init__(self):
        self.json_parser = JsonParser()
        self.user_database_manager = UserDatabaseManager()
        self.message_database_manager = MessageDatabaseManager()
        self.topic_database_manager = TopicDatabaseManager()
        self.transport_layer_buffer = []
        self.message_to_process = []
        self.peer_id = None
        self.timestamp = None

    def __del__(self):
        del self.json_parser
        del self.user_database_manager
        del self.message_database_manager
        del self.topic_database_manager

    def receive_message(self, transport_layer_message, peer_id):
        transport_layer_pdu = []
        if transport_layer_message.get('type') == 'info':
            return_message = self.transport_message_controller(transport_layer_message, peer_id)
            if return_message:
                transport_layer_pdu += return_message

        elif transport_layer_message.get('type') == 'data':
            timestamp = transport_layer_message.get('timestamp')
            for message in transport_layer_message.get('payload'):
                if not self.is_message_type_valid(message):
                    continue
                message['timestamp'] = timestamp
                if message.get('ud'):
                    message['ud'] = str(message.get('ud'))

                return_messsage = self.application_message_controller(message, peer_id)
                if return_messsage:
                    transport_layer_pdu += return_messsage

        return transport_layer_pdu

    def application_message_controller(self, msg_payload, peer_id):
        if msg_payload.get('msg_type') == 0:
            return self.connection_request(msg_payload, peer_id)
        elif msg_payload.get('msg_type') == 2:
            return self.subscribe_request(msg_payload)
        elif msg_payload.get('msg_type') == 4:
            return self.unsubscribe_request(msg_payload)
        elif msg_payload.get('msg_type') == 6:
            return self.publish_request(msg_payload)
        elif msg_payload.get('msg_type') == 8:
            return self.create_topic(msg_payload)
        elif msg_payload.get('msg_type') == 10:
            return self.disconnect_request(msg_payload)
        else:
            pass

    def transport_message_controller(self, msg_payload, peer_id):
        self.set_peer_id(peer_id)
        if msg_payload.get('event') == 'disconnected':
            self.user_accidentally_disconnected(msg_payload, peer_id)
        elif msg_payload.get('event') == 'reconnected':
            overdue_messages = []
            if self.message_database_manager.is_peer_to_update(self.get_peer_id()):
                overdue_messages = self.get_overdue_messages(msg_payload)
            return overdue_messages

    def is_message_type_valid(self, msg_payload):
        if msg_payload.get('msg_type') is not None and msg_payload.get('msg_type') <= 10 and msg_payload.get(
                'msg_type') % 2 == 0:
            return True
        elif msg_payload.get('type') == 'info':
            return True
        else:
            return False

    def connection_request(self, msg, peer_id):
        if self.user_database_manager.client_authenticated(msg.get('id'), msg.get('pwd')):
            if self.user_database_manager.get_peer_id_from_id(msg.get('id')) != peer_id:
                msg['id'] = 0
        else:
            self.user_database_manager.register_client(self.get_peer_id(), msg.get('id'), msg.get('pwd'), msg.get('l'))
        return [self.connection_ack(msg)]

    def connection_ack(self, msg):
        msg['msg_type'] = 1
        transport_layer_data = self.json_parser.wrap_response(msg, self.get_peer_id())
        return transport_layer_data

    def disconnect_request(self, msg):
        transport_layer_data = []
        messages = []
        if self.user_database_manager.client_authenticated(msg.get('id'), msg.get('pwd')):
            topic_object = self.user_database_manager.get_all_client_topics(msg.get('id'))
            if topic_object:
                subscribers = []
                for to in topic_object:
                    topic_name = to.topic_name

                    for subscriber_object in self.user_database_manager.get_subscribers(topic_name):
                        if subscriber_object.client_id not in subscribers \
                                and subscriber_object.client_id != msg.get('id'):
                            subscribers.append(subscriber_object.client_id)

                    self.user_database_manager.remove_from_topic(msg.get('id'), topic_name)
                    if self.message_database_manager.is_to_updade(msg.get('id'), topic_name):
                        self.message_database_manager.remove_from_update(msg.get('id'), topic_name)
                        self.user_database_manager.remove_from_topic(msg.get('id'), topic_name)
                        if self.topic_database_manager.topics_organiser(msg.get('id'), topic_name):
                            if not self.user_database_manager.are_subscribers(topic_name):
                                self.topic_database_manager.remove_topic(topic_name)
                                self.message_database_manager.remove_all_messages(topic_name, datetime.min,
                                                                                  datetime.max)
                            else:
                                subscriber = self.user_database_manager.get_subscribers(topic_name)
                                self.topic_database_manager.change_organiser(subscriber[0].client_id, msg.get('top'))

                transport_layer_data += self.construct_lwt_messages(msg, subscribers)
            msg['sr'] = 0
            messages.append(self.disconnect_response(msg))
            self.user_database_manager.unregister_client(msg.get('id'), msg.get('pwd'))
        transport_layer_data += messages
        return transport_layer_data

    def disconnect_response(self, msg):
        msg_copy = copy.deepcopy(msg)
        msg_copy['msg_type'] = 11
        transport_layer_data = self.json_parser.wrap_response(msg_copy, self.peer_id)
        return transport_layer_data

    def construct_lwt_messages(self, msg, subscribers):
        transport_layer_message = []
        lwt_message = self.user_database_manager.get_lwt(msg.get('id'))
        for subscriber in subscribers:
            message = dict()
            message['msg_type'] = 12
            message['id'] = subscriber
            message['top'] = "lwt message"
            message['tp'] = msg.get('tp')
            message['ud'] = lwt_message
            message['timestamp'] = datetime.now()
            message['s'] = copy.deepcopy(msg.get('id'))
            transport_layer_message += self.json_parser.wrap_message([message], subscriber,
                                       self.user_database_manager.get_peer_id_from_id(subscriber))

        return transport_layer_message

    def create_topic(self, msg):
        if not self.user_database_manager.client_exist(msg.get('id')):
            msg['ches'] = 2
        else:
            if not self.user_database_manager.client_authenticated(msg.get('id'), msg.get('pwd')):
                msg['ches'] = 3
            else:
                if self.topic_database_manager.exist_topic(msg.get('top')):
                    msg['ches'] = 1
                else:
                    self.topic_database_manager.add_topic(msg.get('id'), msg.get('top'), msg.get('tp'))
                    self.user_database_manager.add_to_topic(msg.get('id'), msg.get('top'), msg.get('ku'),
                                                            msg.get('timestamp'))
                    if msg.get('ku'):
                        self.message_database_manager.add_user(self.get_peer_id(), msg.get('id'), msg.get('top'),
                                                               msg.get('timestamp'))
                    msg['ches'] = 0

        return [self.create_topic_response(msg)]

    def create_topic_response(self, msg):
        msg['msg_type'] = 9
        transport_layer_data = self.json_parser.wrap_response(msg, self.peer_id)
        return transport_layer_data

    def subscribe_request(self, msg):
        if not self.user_database_manager.client_exist(msg.get('id')):
            msg['sr'] = 2
        else:
            if not self.user_database_manager.client_authenticated(msg.get('id'), msg.get('pwd')):
                msg['sr'] = 3
            else:
                if not self.topic_database_manager.exist_topic(msg.get('top')):
                    msg['sr'] = 1
                else:
                    msg['sr'] = 0
                    self.user_database_manager.add_to_topic(msg.get('id'),
                                                            msg.get('top'),
                                                            msg.get('ku'),
                                                            msg.get('timestamp'))
                    if msg.get('ku'):
                        self.message_database_manager.add_user(self.get_peer_id(),
                                                               msg.get('id'),
                                                               msg.get('top'),
                                                               msg.get('timestamp'))
        msg.pop('ku', None)
        return [self.subscribe_response(msg)]

    def subscribe_response(self, msg):
        msg['msg_type'] = 3
        transport_layer_data = self.json_parser.wrap_response(msg, self.peer_id)
        return transport_layer_data

    def unsubscribe_request(self, msg):
        if not self.user_database_manager.client_exist(msg.get('id')):
            msg['sr'] = 2
        else:
            if not self.user_database_manager.client_authenticated(msg.get('id'), msg.get('pwd')):
                msg['sr'] = 3
            else:
                if self.user_database_manager.is_subscriber(msg.get('id'), msg.get('top')):
                    self.user_database_manager.remove_from_topic(msg.get('id'), msg.get('top'))
                    if self.message_database_manager.is_to_updade(msg.get('id'), msg.get('top')):
                        self.message_database_manager.remove_from_update(msg.get('id'), msg.get('top'))
                    if self.topic_database_manager.topics_organiser(msg.get('id'), msg.get('top')):
                        if not self.user_database_manager.are_subscribers(msg.get('top')):
                            self.topic_database_manager.remove_topic(msg.get('top'))
                            self.message_database_manager.remove_all_messages(msg.get('top'), datetime.min,
                                                                              datetime.max)
                        else:
                            subscriber = self.user_database_manager.get_subscribers(msg.get('top'))
                            self.topic_database_manager.change_organiser(subscriber[0].client_id, msg.get('top'))
                    msg['sr'] = 0
        return [self.unsubscribe_response(msg)]

    def unsubscribe_response(self, msg):
        msg['msg_type'] = 5
        transport_layer_data = self.json_parser.wrap_response(msg, self.peer_id)
        return transport_layer_data

    def publish_request(self, msg):
        msg_for_publish_response = copy.deepcopy(msg)
        messages = []
        if not self.user_database_manager.client_exist(msg.get('id')):
            msg_for_publish_response['pr'] = 2
            msg_for_publish_response.pop('ud', None)
        else:
            if not self.user_database_manager.client_authenticated(msg.get('id'), msg.get('pwd')):
                msg_for_publish_response['pr'] = 3
                msg_for_publish_response.pop('ud', None)
            else:
                if not self.user_database_manager.is_subscriber(msg.get('id'), msg.get('top')):
                    msg_for_publish_response['pr'] = 1
                    msg_for_publish_response.pop('ud', None)
                else:
                    messages += self.subscribers_messages(msg)
                    self.user_database_manager.update_activity(msg.get('id'), msg.get('top'), msg.get('timestamp'))
                    msg_for_publish_response['pr'] = 0

        messages.append(self.publish_response(msg_for_publish_response))
        return messages

    def publish_response(self, msg):
        msg['msg_type'] = 7
        transport_layer_data = self.json_parser.wrap_response(msg, self.peer_id)
        return transport_layer_data

    def subscribers_messages(self, msg):
        msg['msg_type'] = 12
        msg['s'] = msg.get('id')
        topic_objects = self.user_database_manager.get_subscribers(msg.get('top'))
        transport_layer_data = []
        for to in topic_objects:
            subscriber = to.client_id
            subscriber_peer_id = self.user_database_manager.get_peer_id_from_id(subscriber)
            if subscriber == msg.get('id'):
                continue
            if self.message_database_manager.is_client_connected(subscriber):
                msg_copy = copy.deepcopy(msg)
                transport_layer_data += self.json_parser.wrap_message([msg_copy], subscriber, subscriber_peer_id)
            elif self.message_database_manager.is_to_updade(subscriber, msg.get('top')):
                self.message_database_manager.add_message(msg)

        return transport_layer_data

    def update_last_activity(self, msg):
        self.user_database_manager.update_activity(msg.get('id'), msg.get('top'), msg.get('timestamp'))

    def change_client_status_to_disconnected(self):
        topics = self.message_database_manager.get_all_subscribed_topics(self.get_peer_id())
        for topic in topics:
            self.message_database_manager.change_status_to_disconnected(self.get_peer_id(), topic)

    def change_client_status_to_connected(self):
        topics = self.message_database_manager.get_all_subscribed_topics(self.get_peer_id())
        for topic in topics:
            self.message_database_manager.change_status_to_connected(self.get_peer_id(), topic)

    def set_disconnection_time(self, msg):
        self.message_database_manager.set_disconnection_time(self.get_peer_id(), msg.get('timestamp'))

    def get_overdue_message(self, msg):
        msg['msg_type'] = 12
        subscribed_topics = self.message_database_manager.get_all_subscribed_topics(self.get_peer_id())
        disconnection_timestamp = self.message_database_manager.get_disconnection_time(self.get_peer_id())
        reconnection_timestamp = msg.get('timestamp')
        overdue_messages = []
        for subscribed_topic in subscribed_topics:
            overdue_messages += self.message_database_manager.get_all_messages(subscribed_topic,
                                                                               disconnection_timestamp,
                                                                               reconnection_timestamp)

        for topic in subscribed_topics:
            self.message_database_manager.change_status_to_connected(self.get_peer_id(), topic)

        if overdue_messages:
            client_id = self.user_database_manager.get_id_from_peer_id(self.get_peer_id())
            messages_reconstructor = []
            for overdue_message in overdue_messages:
                message = dict()
                message['msg_type'] = 12
                message['id'] = overdue_message.sender_id
                message['top'] = overdue_message.topic_name
                message['tp'] = overdue_message.transport_layer_protocol
                message['ud'] = overdue_message.content
                message['timestamp'] = overdue_message.timestamp
                message['s'] = overdue_message.sender_id
                messages_reconstructor.append(message)

            transport_layer_data = self.json_parser.wrap_message(messages_reconstructor,
                                                                 client_id,
                                                                 self.get_peer_id())

            return transport_layer_data

        return None

    def user_accidentally_disconnected(self, msg_payload, peer_id):
        if self.message_database_manager.is_peer_to_update(peer_id):
            self.change_client_status_to_disconnected()
            self.set_disconnection_time(msg_payload)

    def get_overdue_messages(self, msg_payload):
        overdue_messages = self.get_overdue_message(msg_payload)
        self.change_client_status_to_connected()
        subscribed_topics = self.message_database_manager.get_all_subscribed_topics(self.get_peer_id())
        for subscribed_topic in subscribed_topics:
            earliest_disconnection_timestamp = \
                self.message_database_manager.get_earliest_disconnection_timestamp(subscribed_topic)
            self.message_database_manager.remove_all_messages(subscribed_topic, datetime.min,
                                                              earliest_disconnection_timestamp)

        return overdue_messages

    def handle_transport_layer_backup(self, not_send_data, peer_id):
        print("Handle not send message: ", not_send_data)
        messages = not_send_data.get('payload')
        if self.message_database_manager.is_peer_to_update(peer_id):
            for message in messages:
                if not self.message_database_manager.is_client_connected(message.get('id')):
                    self.change_client_status_to_disconnected()
                    self.set_disconnection_time(message)
                self.message_database_manager.add_overdue_message_from_transport_layer(message)

    def set_peer_id(self, peer_id):
        self.peer_id = peer_id

    def set_timestamp(self, timestamp):
        self.timestamp = timestamp

    def get_peer_id(self):
        return self.peer_id

    def get_timestamp(self):
        return self.timestamp
