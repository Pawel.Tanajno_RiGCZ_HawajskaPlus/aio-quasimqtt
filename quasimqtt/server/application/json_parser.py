import json


class JsonParser:

    def get_payload(self, transport_payload):
        data = json.loads(transport_payload)
        transport_protocol, nested_json = list(data.keys())[0], list(data.values())[0]
        client_id, payload = list(nested_json.keys())[0], list(nested_json.values())[0]
        return payload

    def wrap_message(self, msg, subscriber, peer_id):
        return_messages = []
        return_dict = {}

        for message in msg:
            message.pop('id', None)
            message.pop('psw', None)
            message['id'] = subscriber
        if msg:
            return_dict = {msg[0].get('tp'): {peer_id: msg}}

            if msg[0].get('tp') == "TCP":
                return_dict["UDP"] = {}
            else:
                return_dict["TCP"] = {}

        return_messages.append(return_dict)
        return return_messages

    def wrap_response(self, msg, peer_id):
        message = {msg.get('tp'): {peer_id: [msg]}}
        if msg.get('tp') == "TCP":
            message["UDP"] = {}
        else:
            message["TCP"] = {}
        wrapped_message = message
        return wrapped_message
