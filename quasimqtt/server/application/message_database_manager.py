import os
from sqlalchemy import Integer, Column, String, Boolean, DateTime, create_engine, exc, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from sqlalchemy.pool import NullPool

BaseModel = declarative_base()


class KeepUpdate(BaseModel):
    __tablename__ = 'keep_update_table'
    peer_id = Column(Integer, nullable=False)
    client_id = Column(String, primary_key=True, nullable=False)
    topic_name = Column(String(), primary_key=True, nullable=False)
    is_connected = Column(Boolean, nullable=False)
    timestamp = Column(DateTime())


class Messages(BaseModel):
    __tablename__ = 'messages_table'
    topic_name = Column(String, primary_key=True, nullable=False)
    sender_id = Column(String(), primary_key=True, nullable=False)
    content = Column(String(), primary_key=True, nullable=False)
    timestamp = Column(DateTime(), primary_key=True, nullable=False)
    transport_layer_protocol = Column(String(), nullable=False)


class MessageDatabaseManager:
    def __init__(self):
        self.message_database_name = 'message.db'
        if os.path.exists(self.message_database_name):
            os.remove(self.message_database_name)
        self.message_database = create_engine('sqlite:///' + self.message_database_name, poolclass=NullPool)

        BaseModel.metadata.create_all(self.message_database)
        self.db_session = sessionmaker(bind=self.message_database)
        self.session = self.db_session()

    def __del__(self):
        KeepUpdate.__table__.drop(self.message_database)
        Messages.__table__.drop(self.message_database)
        # self.session.commit()
        # self.session.close()

    def add_message(self, msg):
        self.session = self.db_session()
        try:
            self.session.add_all([
                Messages(topic_name=msg.get('top'),
                         sender_id=msg.get('s'),
                         content=msg.get('ud'),
                         timestamp=msg.get('timestamp'),
                         transport_layer_protocol=msg.get('tp')), ])
            self.session.commit()
            self.session.close()
        except exc.IntegrityError:
            self.session.rollback()
        except InterruptedError:
            pass

    def add_overdue_message_from_transport_layer(self, msg):
        self.session = self.db_session()
        try:
            self.session.add_all([
                Messages(topic_name=msg.get('top'),
                         sender_id=msg.get('s'),
                         content=msg.get('ud'),
                         timestamp=msg.get('timestamp'),
                         transport_layer_protocol=msg.get('tp')), ])
            self.session.commit()
            self.session.close()
        except exc.IntegrityError:
            self.session.rollback()
        except InterruptedError:
            pass

    def add_user(self, peer_id, client_id, topic_name, timestamp):
        self.session = self.db_session()
        try:
            self.session.add_all([
                KeepUpdate(peer_id=peer_id, client_id=client_id, topic_name=topic_name, is_connected=True,
                           timestamp=timestamp), ])
            self.session.commit()
        except InterruptedError:
            pass
        except exc.IntegrityError:
            pass

    def is_to_updade(self, client_id, topic_name):
        self.session = self.db_session()
        exist = self.session.query(KeepUpdate).filter(KeepUpdate.client_id == client_id).filter(
            KeepUpdate.topic_name == topic_name).scalar()
        self.session.commit()
        if exist is None:
            return False
        else:
            return True

    def remove_from_update(self, client_id, topic_name):
        self.session = self.db_session()
        self.session.query(KeepUpdate).filter(KeepUpdate.client_id == client_id).filter(
            KeepUpdate.topic_name == topic_name).delete()
        self.session.commit()

    def remove_messages(self, topic_name):
        self.session = self.db_session()
        self.session.query(Messages).filter(Messages.topic_name == topic_name).delete()
        self.session.commit()

    def update_activity(self, client_id, topic_name, activity_time):
        self.session = self.db_session()
        inst_activity = self.session.query(KeepUpdate).filter(KeepUpdate.client_id == client_id). \
            filter(KeepUpdate.topic_name == topic_name).scalar()
        try:
            inst_activity.last_activity = activity_time
        except AttributeError:
            print("update activity - record not found")
        self.session.commit()

    def get_all_messages(self, topic_name, timestamp_start, timestamp_stop):
        result = []
        if timestamp_start:
            self.session = self.db_session()
            inst_activity = self.session.query(Messages).filter(
                and_(Messages.topic_name == topic_name, Messages.timestamp >= timestamp_start,
                     Messages.timestamp <= timestamp_stop)).all()
            # self.session.commit()
            try:
                for ins_act in inst_activity:
                    result.append(ins_act)
            except AttributeError:
                print("update activity - record not found")
        return result

    def remove_all_messages(self, topic_name, timestamp_start, timestamp_stop):
        self.session = self.db_session()
        self.session.query(Messages).filter(Messages.topic_name == topic_name). \
            filter(Messages.timestamp.between(timestamp_start, timestamp_stop)).delete(synchronize_session=False)
        self.session.commit()

    def change_status_to_disconnected(self, peer_id, topic_name):
        self.session = self.db_session()
        keep_update_objects = self.session.query(KeepUpdate).filter(KeepUpdate.peer_id == peer_id).filter(
            KeepUpdate.topic_name == topic_name).all()
        for kuo in keep_update_objects:
            kuo.is_connected = False
        self.session.commit()

    def change_status_to_connected(self, peer_id, topic_name):
        self.session = self.db_session()
        keep_update_objects = self.session.query(KeepUpdate).filter(KeepUpdate.peer_id == peer_id).filter(
            KeepUpdate.topic_name == topic_name).all()
        for kuo in keep_update_objects:
            kuo.is_connected = True
        self.session.commit()

    def is_client_connected(self, client_id):
        self.session = self.db_session()
        keep_updata_object = self.session.query(KeepUpdate).filter(KeepUpdate.client_id == client_id).first()
        if keep_updata_object and keep_updata_object.is_connected:
            return True
        else:
            return False

    def get_all_subscribed_topics(self, peer_id):
        self.session = self.db_session()
        topics = []
        keep_update_objects = self.session.query(KeepUpdate).filter(KeepUpdate.peer_id == peer_id).all()
        for kuo in keep_update_objects:
            topics.append(kuo.topic_name)
        return topics

    def set_disconnection_time(self, peer_id, timestamp):
        self.session = self.db_session()
        disconnected_record = self.session.query(KeepUpdate).filter(KeepUpdate.peer_id == peer_id).all()
        for dr in disconnected_record:
            dr.timestamp = timestamp
        self.session.commit()

    def is_peer_to_update(self, peer_id):
        self.session = self.db_session()
        record_to_update = self.session.query(KeepUpdate).filter(KeepUpdate.peer_id == peer_id).all()
        if record_to_update is None:
            return False
        else:
            return True

    def get_disconnection_time(self, peer_id):
        self.session = self.db_session()
        disconnection_time = None
        keep_update_objects = self.session.query(KeepUpdate).filter(KeepUpdate.peer_id == peer_id).all()
        for kuo in keep_update_objects:
            if disconnection_time is None or kuo.timestamp > disconnection_time:
                disconnection_time = kuo.timestamp
        return disconnection_time

    def get_earliest_disconnection_timestamp(self, topic):
        self.session = self.db_session()
        keep_update_objects = self.session.query(KeepUpdate).filter(KeepUpdate.topic_name == topic).all()
        earliest_disconnection_time = datetime.max
        for kuo in keep_update_objects:
            if kuo.is_connected == False and kuo.timestamp < earliest_disconnection_time:
                earliest_disconnection_time = kuo.timestamp

        return earliest_disconnection_time


'''
message_database_manager = MessageDatabaseManager()
print("is_to_update, without the user at the particular channel: ",
      message_database_manager.is_to_updade("Karol", "temperature"))
print("Is_client_connected, should be False: ", message_database_manager.is_client_connected("Karol"))
message_database_manager.add_user(3124124, "Karol", "temperature", datetime.now())
message_database_manager.add_user(31512521, "Michał", "temperature", datetime.now())
print("is_to_update, with the user Karol at the particular channel: ",
      message_database_manager.is_to_updade("Karol", "temperature"))
print("Is_client_connected, should be True: ", message_database_manager.is_client_connected("Karol"))
message_database_manager.remove_from_update("Karol", "temperature")
print("is_to_update, without the user at the particular channel: ",
      message_database_manager.is_to_updade("Karol", "temperature"))
message_database_manager.add_user(31251253, "Karol", "temperature", datetime.now())
print("get_all_messages, there shoudn't by any message: ",
      message_database_manager.get_all_messages("temperature", datetime.min, datetime.max))
message_database_manager.add_message({"id": "Ola", "top": "temperature", "ud": "26.5", "timestamp": datetime.now(), "tp": "TCP"})
message_database_manager.add_message({"id": "Richard", "top": "temperature", "ud": "56.5", "timestamp": datetime.now(), "tp": "TCP"})
lista_wiadomosci = message_database_manager.get_all_messages("temperature", datetime.min, datetime.max)
print("get all messages, there should be an message: ",
      message_database_manager.get_all_messages("temperature", datetime.min, datetime.max))
print("get all messages, start time is None: ",
      message_database_manager.get_all_messages("temperature", None, datetime.now()))
message_database_manager.remove_all_messages("temperature", datetime.min, datetime.max)
print("get all messages, there shoudn't be any message: ",
      message_database_manager.get_all_messages("temperature", datetime.min, datetime.max))
message_database_manager.update_activity("Karol", "temperature", datetime.now())
message_database_manager.add_user(31251253, "Karol", "humidity", datetime.now())
print("get all subscribed topics: ", message_database_manager.get_all_subscribed_topics(31251253))
print('get disconnection time: ', message_database_manager.get_disconnection_time(31251253))
message_database_manager.change_status_to_disconnected(31251253, "temperature")
message_database_manager.change_status_to_connected(31251253, "temperature")
print("End of tests")
#print(datetime.now())
#print(datetime.utcnow())
#print(type(datetime.now()))
#print(type(datetime.utcnow()))
'''
