import os
from sqlalchemy import Integer, Column, String, Boolean, DateTime, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.exc import IntegrityError
from datetime import datetime

BaseModel = declarative_base()


class Authentication(BaseModel):
    __tablename__ = 'authentication_table'
    peer_id = Column(Integer, primary_key=True, nullable=False)
    client_id = Column(String, primary_key=True, nullable=False)
    password = Column(String, nullable=False)
    last_will_and_testament = Column(String, default='')


class Topic(BaseModel):
    __tablename__ = 'topic_table'
    client_id = Column(String, primary_key=True, nullable=False)
    topic_name = Column(String(), primary_key=True, nullable=False)
    keep_updated = Column(Boolean, nullable=False)
    last_activity = Column(DateTime())


class UserDatabaseManager:
    def __init__(self):
        self.user_database_name = 'user.db'
        if os.path.exists(self.user_database_name):
            os.remove(self.user_database_name)
        self.user_database = create_engine('sqlite:///' + self.user_database_name, poolclass=NullPool)

        BaseModel.metadata.create_all(self.user_database)
        self.db_session = sessionmaker(bind=self.user_database, expire_on_commit=False)
        self.session = self.db_session()

    def __del__(self):
        try:
            Authentication.__table__.drop(self.user_database)
            Topic.__table__.drop(self.user_database)
            self.session.commit()
            self.session.close()
        except ImportError:
            pass

    def client_authenticated(self, client_id, password):
        self.session = self.db_session()
        exist = self.session.query(Authentication).filter(Authentication.client_id == client_id).filter(
            Authentication.password == password).scalar()
        self.session.commit()
        if exist is None:
            return False
        else:
            return True

    def client_exist(self, client_id):
        self.session = self.db_session()
        exist = self.session.query(Authentication).filter(Authentication.client_id == client_id).scalar()
        self.session.commit()
        if exist is None:
            return False
        else:
            return True

    def register_client(self, peer_id, client_id, password, l):
        self.session = self.db_session()
        try:
            self.session.add_all([
                Authentication(peer_id=peer_id, client_id=client_id, password=password, last_will_and_testament=l), ])
            self.session.commit()
        except IntegrityError:
            pass

    def unregister_client(self, client_id, password):
        self.session = self.db_session()
        self.session.query(Authentication).filter(Authentication.client_id == client_id).\
            filter(Authentication.password == password).delete()
        self.session.commit()

    def add_to_topic(self, client_id, topic_name, keep_updated, last_activity):
        self.session = self.db_session()
        try:
            self.session.add_all([
                Topic(client_id=client_id, topic_name=topic_name, keep_updated=bool(keep_updated),
                      last_activity=last_activity), ])
            self.session.commit()
        except IntegrityError:
            pass

    def are_subscribers(self, topic_name):
        self.session = self.db_session()
        try:
            exist = self.session.query(Topic).filter(Topic.topic_name == topic_name).all()
            self.session.commit()
        except MultipleResultsFound:
            print("Multiple results found")
        except NoResultFound:
            print("No results found")
        else:
            if not exist:
                return False
            else:
                return True

    def get_subscribers(self, topic_name):
        self.session = self.db_session()
        try:
            exist = self.session.query(Topic).filter(Topic.topic_name == topic_name).all()
            self.session.commit()
        except MultipleResultsFound:
            print("Multiple results found")
        except NoResultFound:
            print("No results found")
        else:
            if exist is None:
                return False
            else:
                return exist

    def remove_client_records(self, client_id):
        self.session = self.db_session()
        self.session.query(Topic).filter(Topic.client_id == client_id).delete()
        self.session.commit()

    def remove_from_topic(self, client_id, topic_name):
        self.session = self.db_session()
        self.session.query(Topic).filter(Topic.client_id == client_id).filter(Topic.topic_name == topic_name).delete()
        self.session.commit()

    def is_subscriber(self, client_id, topic_name):
        self.session = self.db_session()
        exist = self.session.query(Topic).filter(Topic.client_id == client_id) \
            .filter(Topic.topic_name == topic_name).scalar()
        self.session.commit()
        if exist is None:
            return False
        else:
            return True

    def update_activity(self, client_id, topic_name, activity_time):
        self.session = self.db_session()
        inst_activity = self.session.query(Topic).filter(Topic.client_id == client_id). \
            filter(Topic.topic_name == topic_name).scalar()
        try:
            inst_activity.last_activity = activity_time
        except AttributeError:
            print("update activity - record not found")
        self.session.commit()

    def get_all_client_topics(self, client_id):
        self.session = self.db_session()
        try:
            exist = self.session.query(Topic).filter(Topic.client_id == client_id).all()
            self.session.commit()
        except MultipleResultsFound:
            print("Multiple results found")
        except NoResultFound:
            print("No results found")
        else:
            if not exist:
                return False
            else:
                return exist

    def get_peer_id_from_id(self, client_id):
        self.session = self.db_session()
        try:
            authentication_table_record = \
                self.session.query(Authentication).filter(Authentication.client_id == client_id).first()
            if authentication_table_record:
                return authentication_table_record.peer_id
        except MultipleResultsFound:
            print("Multiple results found")
        except NoResultFound:
            print("No results found")

        return None

    def get_id_from_peer_id(self, peer_id):
        self.session = self.db_session()
        try:
            authentication_table_record = \
                self.session.query(Authentication).filter(Authentication.peer_id == peer_id).first()
            if authentication_table_record:
                return authentication_table_record.client_id
        except MultipleResultsFound:
            print("Multiple results found")
        except NoResultFound:
            print("No results found")

        return None

    def get_lwt(self, client_id):
        self.session = self.db_session()
        try:
            authentication_table_record = \
                self.session.query(Authentication).filter(Authentication.client_id == client_id).first()
            if authentication_table_record:
                return authentication_table_record.last_will_and_testament
        except MultipleResultsFound:
            print("Multiple results found")
        except NoResultFound:
            print("No results found")

        return None


'''
user_database_manager = UserDatabaseManager()
print("Is Karol Authenticated: ", user_database_manager.client_authenticated('Karol', '123456'))
user_database_manager.register_client(99999999, 'Karol', '123456', 'Karol sie rozlaczyl')
print("Does client Karol exist?: " ,user_database_manager.client_exist('Karol'))
print("Does client Michał exist?: ", user_database_manager.client_exist('Michal'))
print("Is Karol Authenticated?: ", user_database_manager.client_authenticated('Karol', '123456'))
print("Get Karol's lwt: ", user_database_manager.get_lwt('Karol'))
print("Is Michal Authenticated?: ", user_database_manager.client_authenticated('Michal', '123456'))
print("Does Karol subscribe the temperature channel?: ",user_database_manager.is_subscriber("Karol", "temperature"))
print("Is there any subsciber at the temperature channel?: ",user_database_manager.are_subscribers("temperature"))
user_database_manager.add_to_topic("Karol", "temperature", 'False', datetime.now())
user_database_manager.add_to_topic("Ola", "temperature", 'False', datetime.now())
print("Does Karol subscribe the temperature channel?: ",user_database_manager.is_subscriber("Karol", "temperature"))
print("Is there any subsciber at the temperature channel?: ", user_database_manager.are_subscribers("temperature"))
print("Get subscribers of temperature channel: ", user_database_manager.get_subscribers("temperature"))
user_database_manager.update_activity("Karol", "temperature", datetime.now())
user_database_manager.remove_client_records("Karol")
print("Is there any subsciber at the temperature channel?: ",user_database_manager.is_subscriber("Karol", "temperature"))
print("Is there any subsciber at the temperature channel?: ",user_database_manager.are_subscribers("temperature"))
e = user_database_manager.get_subscribers("temperature")
user_database_manager.update_activity("Karol", "temperature", datetime.now())
print("Karol peer_id is: ", user_database_manager.get_peer_id_from_id("Karol"))
print("get_id_from_peer_id of_Karol: ", user_database_manager.get_id_from_peer_id(99999999))
print("get_id_from_peer_id of unknown peer_id: ", user_database_manager.get_id_from_peer_id(99999))
user_database_manager.unregister_client("Karol", '123456')
print("Is Karol Authenticated, after unregistering: ", user_database_manager.client_authenticated('Karol', '123456'))
'''
