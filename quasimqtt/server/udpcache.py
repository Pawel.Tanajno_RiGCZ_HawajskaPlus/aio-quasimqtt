from collections import defaultdict


class UDPCache:
    def __init__(self):
        self.cache = defaultdict(list)

    def add_message_to_cache(self, peer_name, message):
        self.cache[peer_name].append(message)

    def clear_cache(self, peer_name):
        self.cache[peer_name].clear()

    def is_empty(self, peer_name):
        return peer_name not in self.cache or not self.cache[peer_name]
