import time
from collections import defaultdict


class ClientStorage:
    def __init__(self):
        self.tcp_transport_register = {}
        self.udp_peer_register = set()

        self.last_seen = {
            "TCP": {},
            "UDP": {}
        }

        self.peers = defaultdict(dict)
        self.app_connections = {
            "TCP": set(),
            "UDP": set()
        }

    def add_peer_tcp(self, peer_name, transport):
        self.tcp_transport_register[peer_name] = transport

    def add_peer_udp(self, peer_name):
        self.udp_peer_register.add(peer_name)

    def remove_peer_tcp(self, peer_name):
        self.tcp_transport_register.pop(peer_name)

    def remove_peer_udp(self, peer_name):
        self.udp_peer_register.remove(peer_name)

    def remove_peer(self, peer_name, protocol):
        if protocol == "TCP":
            self.remove_peer_tcp(peer_name)
        else:
            self.remove_peer_udp(peer_name)
        if peer_name in self.last_seen[protocol]:
            self.last_seen[protocol].pop(peer_name)

    def add_app_connection(self, peer_id, protocol):
        if peer_id not in self.app_connections[protocol]:
            self.app_connections[protocol].add(peer_id)

    def remove_app_connection(self, peer_id, protocol):
        if peer_id in self.app_connections[protocol]:
            self.app_connections[protocol].remove(peer_id)

    def _is_available_tcp(self, peer_name):
        return peer_name in self.tcp_transport_register

    def _is_available_udp(self, peer_name):
        return peer_name in self.udp_peer_register

    def is_available(self, peer_name, protocol):
        if protocol == "TCP":
            return self._is_available_tcp(peer_name)
        else:
            return self._is_available_udp(peer_name)

    def is_peer_registered(self, peer_name, protocol):
        for peer_id, connections in self.peers.items():
            if connections.get(protocol, 0) == peer_name:
                return True, peer_id
        return False, 0

    def get_peer_name_by_id(self, peer_id, protocol):
        return self.peers[peer_id][protocol]

    def get_id_by_peer_name(self, peer_name, protocol):
        for peer_id, connections in self.peers.items():
            if connections.get(protocol, 0) == peer_name:
                return peer_id
        return 0

    def get_tcp_transport(self, peer_name):
        return self.tcp_transport_register[peer_name]

    def get_udp_peers(self):
        return self.udp_peer_register

    def register_peer(self, peer_id, peer_name, protocol):
        self.peers[peer_id][protocol] = peer_name

    def register_time(self, peer_name, protocol):
        self.last_seen[protocol][peer_name] = time.time()

    def get_unreachable_udp(self, max_time):
        current_time = time.time()
        unreachable = {k: v for k, v in self.last_seen["UDP"].items() if current_time - v >= max_time}
        self.last_seen["UDP"] = {k: v for k, v in self.last_seen["UDP"].items() if k not in unreachable}
        return unreachable
