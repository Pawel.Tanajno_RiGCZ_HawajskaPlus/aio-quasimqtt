import copy
import logging
from quasimqtt.server.application.mqtt_protocol import MqttApplication
import time

logging.basicConfig(format="%(name)s: %(levelname)s - %(message)s", level=logging.INFO)
logging.getLogger().setLevel(logging.ERROR)


class ServerApplication:
    def __init__(self):
        self.mqtt_application = MqttApplication()
        """
        _application_data format should be: {
                                                "TCP": {
                                                    peer1_id: [{<msg1_payload>}, {<msg2_payload>}, ...],
                                                    peer2_id: [{...}, ...],
                                                    ...
                                                },
                                                "UDP": {
                                                    ...
                                                }
                                            }
        """
        self.data_to_send_q = None
        self._application_data = {
            "TCP": {},
            "UDP": {}
        }
        self.peers = None

        self.logger = logging.getLogger("server-app")
        self.logger.setLevel(logging.INFO)

    async def data_not_sent(self, peer_id, protocol, transport_layer_message):
        """
        Handles data which was managed to send to a specific peer, but wasn't.
        :param peer_id: peer's id given by Server.
        :param protocol: a protocol though which the data was managed to send.
        :param transport_layer_message: actual data.
        :return:
        """
        self.logger.info(f"Handling data {transport_layer_message} which was not sent to {peer_id} over {protocol}...")
        if isinstance(transport_layer_message, dict) and 'payload' in transport_layer_message:
            self.mqtt_application.handle_transport_layer_backup(transport_layer_message, peer_id)
        elif isinstance(transport_layer_message, list):
            for tlm in transport_layer_message:
                if isinstance(tlm, dict) and 'payload' in tlm:
                    self.mqtt_application.handle_transport_layer_backup(tlm, peer_id)
        return

    def consume_data(self, peer_id, protocol, data):
        """
        Handles data which was received from a specific peer.
        :param peer_id: identifier of peer which sent a data.
        :param protocol: a protocol though which the data was sent.
        :param data: actual data derived from peer or system.
        """
        self.logger.info(f"consume data: {data}  from:  {peer_id} by: {protocol}")

        self.logger.info(f"This payload is being send to the MQTT server app: [{data}] from: [{peer_id}]")
        self.mqtt_application.set_peer_id(peer_id)

        processed_data = self.mqtt_application.receive_message(data, peer_id)
        self.logger.info(f"Data send from the applicaiton layer:  {processed_data}")
        for msg in processed_data:
            self.push_data(msg)

    def push_data(self, data=None):
        """
        Manages data which is being sent to connected peers.
        :param data: data to send. Should stick to format of the _application_data attribute
            (see docstring under __init__)
        :return:
        """

        if data is None:
            data = copy.deepcopy(self._application_data)
            self._application_data["TCP"].clear()
            self._application_data["UDP"].clear()
        self.logger.info(f"The MQTT server application sends this message:[{data}]")
        self.data_to_send_q.sync_q.put(data)

    def get_available_peers(self, protocol):
        return {peer_id for peer_id in self.peers if protocol in self.peers[peer_id]}

