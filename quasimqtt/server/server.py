import asyncio
from copy import deepcopy
from datetime import datetime
import janus
import json
import logging

from quasimqtt.config.config import *
from quasimqtt.protocols.prototcp import TCPServerProtocol
from quasimqtt.protocols.protoudp import UDPServerProtocol
from quasimqtt.server.serverapp import ServerApplication
from quasimqtt.server.clientstorage import ClientStorage
from quasimqtt.server.udpcache import UDPCache

logging.basicConfig(format="%(name)s: %(levelname)s - %(message)s", level=logging.DEBUG)


class ServerConnectionManager:
    def __init__(self, loop, server_config, srv_app):
        self.loop = loop

        self.data_q = None
        self.peer_watch_q = None
        self.server_config = server_config
        self.servers = {
            "TCP": TCPServerProtocol(),
            "UDP": UDPServerProtocol()
        }

        self.servers["TCP"].logger.setLevel(logging.DEBUG)
        self.servers["UDP"].logger.setLevel(logging.DEBUG)

        self.client_storage = ClientStorage()
        self.udp_cache = UDPCache()

        self.run_udp_task = None

        self.server_application = srv_app
        self.logger = logging.getLogger("server-manager")
        self.logger.setLevel(logging.DEBUG)

    async def server_setup(self):
        self.data_q = janus.Queue()
        self.peer_watch_q = janus.Queue()
        self.servers["TCP"].data_q = self.data_q
        self.servers["TCP"].peer_watch_q = self.peer_watch_q
        self.servers["UDP"].data_q = self.data_q
        self.servers["UDP"].peer_watch_q = self.peer_watch_q
        self.servers["UDP"].restart_q = janus.Queue()

        self.server_application.data_to_send_q = janus.Queue()
        self.server_application.peers = self.client_storage.peers

        self.loop.create_task(self._ctl())
        self.loop.create_task(self.run_server("TCP"))
        self.run_udp_task = self.loop.create_task(self.run_server("UDP"))
        self.loop.create_task(self.read_data())
        self.loop.create_task(self.pull_data())
        self.loop.create_task(self.watch_peers())
        self.loop.create_task(self.watch_udp_peers_unreachable())
        self.loop.create_task(self.keepalive_udp())
        self.loop.create_task(self._restart_udp())

        self.logger.info("Server setup has been finished!")

    async def shutdown(self):
        self.logger.info("Shutdown in progress...")
        self.logger.info("Finishing job...")
        await self.data_q.async_q.put((None, None, None, True))
        [task.cancel() for task in asyncio.all_tasks() if task is not asyncio.current_task()]
        self.logger.info("Shutdown completed!")

    # RUN SECTION

    async def _open_tcp(self):
        tcp_server = await self.loop.create_server(
            protocol_factory=self.servers["TCP"],
            host=self.server_config["TCP"]["IPV4"],
            port=self.server_config["TCP"]["PORT"]
        )
        self.servers["TCP"].is_closed = self.loop.create_future()
        return tcp_server

    async def _open_udp(self):
        udp_server, _ = await self.loop.create_datagram_endpoint(
            protocol_factory=self.servers["UDP"],
            local_addr=(self.server_config["UDP"]["IPV4"], self.server_config["UDP"]["PORT"])
        )
        self.servers["UDP"].is_closed = self.loop.create_future()
        self.servers["UDP"].restart = self.loop.create_future()
        return udp_server

    async def run_server(self, protocol):
        open_server = self._open_tcp if protocol == "TCP" else self._open_udp
        server = await open_server()
        self.logger.info(f"[{protocol}] ready")
        try:
            await self.servers[protocol].is_closed
        finally:
            await self._shutdown(protocol)
            server.close()
            self.logger.info(f"[{protocol}] closed")

    async def _shutdown(self, protocol):
        self.logger.info(f"[{protocol}] closing connection(s)...")
        if protocol == "TCP":
            for transport in self.client_storage.tcp_transport_register.values():
                transport.close()
        else:
            shutdown_messages = {peer_name: {"type": "ctrl", "event": "leaving"}
                                 for peer_name in self.client_storage.udp_peer_register}
            await self._send(shutdown_messages, "UDP")
            self.servers["UDP"].transport.close()

    async def _restart_udp(self):
        while True:
            await self.servers["UDP"].restart_q.async_q.get()
            self.logger.info("[UDP] restarting server")
            self.run_udp_task.cancel()
            await asyncio.sleep(1)
            self.run_udp_task = self.loop.create_task(self.run_server("UDP"))
            self.logger.info("[UDP] server restarted")

    # SENDING SECTION

    async def _send_tcp(self, peer_name, payload):
        msg = json.dumps(payload) + ","
        self.client_storage.get_tcp_transport(peer_name).write(msg.encode())

    async def _send_udp(self, peer_name, payload):
        msg = json.dumps(payload) + ","
        self.servers["UDP"].transport.sendto(msg.encode(), peer_name)

    async def _send(self, data_to_send: dict, protocol, inform_app=True):
        try:
            send = self._send_tcp if protocol == "TCP" else self._send_udp
            for peer_name, data in data_to_send.items():
                peer_id = self.client_storage.get_id_by_peer_name(peer_name, protocol)
                if not self.client_storage.is_available(peer_name, protocol):
                    self.logger.warning(f"[{protocol}] data {data} has not been send "
                                        f"to {peer_id} ({peer_name}): peer is not available")
                    if inform_app:
                        await self.server_application.data_not_sent(peer_id, protocol, data)
                else:
                    self.logger.debug(f"[{protocol}] Sending data {data} to {peer_id} ({peer_name})")
                    if inform_app and protocol == "UDP":
                        self.udp_cache.add_message_to_cache(peer_name, data)
                    data = self.remove_timestamp_from_payload(data)
                    await send(peer_name, data)
                    self.logger.debug(f"[{protocol}] data {data} sent to {peer_name}")
        except Exception as e:
            self.logger.error(f"Messages can't be sent, reason:\n{str(e)}")

    # HANDLING NEW DATA SECTION

    async def handle_control_data(self, peer_name, protocol, data):
        if data["event"] == "leaving":
            self.disconnect_peer(peer_name, protocol)
        elif data["event"] == "keepalive-client":
            await self._send(
                {peer_name: {"type": "ctrl", "event": "keepalive-client"}},
                protocol,
                inform_app=False
            )
        elif data["event"] == "connection-request":
            peer_id = data["client_id"]
            if not self.client_storage.is_peer_registered(peer_name, protocol)[0]:
                self.client_storage.register_peer(peer_id, peer_name, protocol)
                self.peer_app_interaction(peer_id, protocol, "connected")
                self.logger.info(f"[{protocol}] peer {peer_name} with ID {peer_id} registered")
            elif peer_id not in self.client_storage.app_connections[protocol]:
                self.peer_app_interaction(peer_id, protocol, "reconnected")
                self.logger.info(f"[{protocol}] peer {peer_name} with ID {peer_id} has been seen before")
            self.client_storage.add_app_connection(peer_id, protocol)
            await self._send(
                {peer_name: {"type": "ctrl", "event": "connection-accepted"}},
                protocol,
                inform_app=False
            )

    async def read_data(self):
        while True:
            try:
                peer_name, protocol, data, bk = await self.data_q.async_q.get()
                if bk:
                    break
                self.client_storage.register_time(peer_name, protocol)
                if protocol == "UDP":
                    self.udp_cache.clear_cache(peer_name)
                    self.check_udp_peer_status(peer_name)
                if data["type"] == "ctrl":
                    await self.handle_control_data(peer_name, protocol, data)
                elif data["type"] == "data":
                    self.pass_data_to_app(
                        self.client_storage.get_id_by_peer_name(peer_name, protocol),
                        protocol,
                        data
                    )
            except Exception as e:
                if str(e) != "":
                    self.logger.error(f"Messages can't be read, reason:\n{str(e)}")

    # CONNECTION MAINTENANCE SECTION

    def check_udp_peer_status(self, peer_name):
        if not self.client_storage.is_available(peer_name, "UDP"):
            self.peer_watch_q.sync_q.put((peer_name, None, "UDP", "connected"))

    async def keepalive_udp(self):
        while True:
            await asyncio.sleep(10)
            for peer_name in self.client_storage.get_udp_peers():
                self.logger.debug(f"[UDP] sending keepalive to {peer_name}")
                await self._send({peer_name: {"type": "ctrl", "event": "keepalive"}}, "UDP")

    async def watch_udp_peers_unreachable(self):
        while True:
            await asyncio.sleep(3)
            self.logger.debug("[UDP] checking for peers reachability...")
            unreachable = self.client_storage.get_unreachable_udp(10)
            for peer_name, last_seen in unreachable.items():
                if not self.udp_cache.is_empty(peer_name):
                    data = self.udp_cache.cache[peer_name].copy()
                    self.logger.warning(f"[UDP] attempted to send "
                                        f"data {str(data)[:50]}... "
                                        f" peer {peer_name}, but was unreachable")
                    peer_id = self.client_storage.get_id_by_peer_name(peer_name, "UDP")
                    await self.server_application.data_not_sent(peer_id, "UDP", data)
                    self.udp_cache.clear_cache(peer_name)
                await self.peer_watch_q.async_q.put((peer_name, None, "UDP", "disconnected"))

    async def watch_peers(self):
        while True:
            peer_name, transport, protocol, event = await self.peer_watch_q.async_q.get()
            if event == "connected":
                self.connect_peer(peer_name, transport, protocol)
            elif event == "disconnected":
                self.disconnect_peer(peer_name, protocol)

    def connect_peer(self, peer_name, transport, protocol):
        if protocol == "TCP":
            self.client_storage.add_peer_tcp(peer_name, transport)
        else:
            self.client_storage.add_peer_udp(peer_name)
        self.logger.info(f"[{protocol}] peer {peer_name} transport connected")

    def disconnect_peer(self, peer_name, protocol):
        if protocol == "TCP":  # because for UDP we have always single transport for all peers
            self.client_storage.get_tcp_transport(peer_name).close()
        self.client_storage.remove_peer(peer_name, protocol)
        peer_id = self.client_storage.get_id_by_peer_name(peer_name, protocol)
        self.logger.info(f"[{protocol}] peer {peer_id} ({peer_name}) is unreachable")
        self.peer_app_interaction(peer_id, protocol, "disconnected")
        self.client_storage.remove_app_connection(peer_id, protocol)

    # INTERACTION WITH APP SECTION

    def peer_app_interaction(self, peer_id, protocol, event):
        self.pass_data_to_app(
            peer_id,
            protocol,
            {"type": "info", "event": event})

    def pass_data_to_app(self, peer_id, protocol, data):
        self.logger.debug(f"[{protocol}] passing data {data} from {peer_id} to app")
        self.add_timestamp(data)
        self.server_application.consume_data(peer_id, protocol, data)

    async def pull_data(self):
        while True:
            data = await self.server_application.data_to_send_q.async_q.get()
            tcp_data, udp_data = data["TCP"], data["UDP"]
            if tcp_data:
                await self._send(self._transform_data(tcp_data, "TCP"), "TCP")
            if udp_data:
                await self._send(self._transform_data(udp_data, "UDP"), "UDP")

    def _transform_data(self, data_to_send, protocol):
        data_stamp = {}
        for peer_id, payload in data_to_send.items():
            peer_name = self.client_storage.get_peer_name_by_id(peer_id, protocol)
            data_stamp[peer_name] = {"type": "data", "payload": payload}
        return data_stamp

    # MISC SECTION

    @staticmethod
    async def _ctl():
        while True:
            await asyncio.sleep(5)

    @staticmethod
    def add_timestamp(data):
        timestamp = datetime.now()
        data["timestamp"] = timestamp

    @staticmethod
    def remove_timestamp_from_payload(data):
        data = deepcopy(data)
        data_type = data.get("type")
        if data_type == "data":
            for message in data["payload"]:
                message.pop("timestamp")
        return data


def main():
    loop = asyncio.get_event_loop()
    application = ServerApplication()
    scm = ServerConnectionManager(loop, SERVER_CONFIG, application)

    try:
        loop.create_task(scm.server_setup())
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(scm.shutdown())
        loop.close()


main()
