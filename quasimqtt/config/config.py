SERVER_CONFIG = {
    "TCP": {
        'IPV4': '127.0.0.1',
        'PORT': 8888
    },
    "UDP": {
        'IPV4': '127.0.0.1',
        'PORT': 8889
    }
}