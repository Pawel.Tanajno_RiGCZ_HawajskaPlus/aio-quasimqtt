import json


def construct_proper_json(data):
    data = data.decode()
    if data.endswith(","):
        data = data[:-1]
    data = "[" + data + "]"
    data_json = json.loads(data)
    return data_json
