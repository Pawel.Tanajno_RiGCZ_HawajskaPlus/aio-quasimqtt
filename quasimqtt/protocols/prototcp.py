import asyncio
import logging

from quasimqtt.protocols.utils import construct_proper_json

# logging.basicConfig(format="%(name)s: %(levelname)s - %(message)s")


class TCPServerProtocol(asyncio.Protocol):
    def __init__(self):
        self.data_q = None
        self.peer_watch_q = None

        self.is_closed = None

        self.logger = logging.getLogger("server-tcp")

    def connection_made(self, transport: asyncio.BaseTransport):
        peer_name = transport.get_extra_info('peername')
        transport.set_protocol(TCPServerReceiveProtocol(peer_name,
                                                        transport,
                                                        self.data_q,
                                                        self.peer_watch_q,
                                                        self.logger))
        self.peer_watch_q.sync_q.put((peer_name, transport, "TCP", "connected"))
        self.logger.info(f"Connection from {peer_name}")

    def __call__(self):
        return self


class TCPServerReceiveProtocol(asyncio.Protocol):
    def __init__(self, peer_name, transport, data_q, peer_watch_q, logger):
        self.transport = transport
        self.peer_name = peer_name
        self.data_q = data_q
        self.peer_watch_q = peer_watch_q
        self.logger = logger

    def data_received(self, data):
        self.logger.debug(f"Received data {data} from {self.peer_name}")
        data = construct_proper_json(data)
        for message in data:
            self.data_q.sync_q.put((self.peer_name, "TCP", message, None))

    def connection_lost(self, exc):
        self.peer_watch_q.sync_q.put((self.peer_name, self.transport, "TCP", "disconnected"))


class TCPClientProtocol(asyncio.Protocol):
    def __init__(self):
        self.data_q = None

        self.is_closed = None
        self.transport = None

        self.logger = logging.getLogger("client-tcp")

    def connection_made(self, transport):
        self.logger.info(f"Successfully connected with server: {transport.get_extra_info('peername')}")
        self.transport = transport

    def data_received(self, data):
        self.logger.debug(f"Received data {data} from server")
        data = construct_proper_json(data)
        for message in data:
            self.data_q.sync_q.put(("TCP", message, None))

    def connection_lost(self, exc):
        self.logger.info('Connection with Server has been closed')
        try:
            self.is_closed.set_result(True)
        except asyncio.InvalidStateError:
            pass

    def __call__(self):
        return self
