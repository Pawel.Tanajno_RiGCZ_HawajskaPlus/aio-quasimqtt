import asyncio
import json
import logging

from quasimqtt.protocols.utils import construct_proper_json

# logging.basicConfig(format="%(name)s: %(levelname)s - %(message)s")


class UDPServerProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        self.data_q = None
        self.peer_watch_q = None
        self.restart_q = None

        self.is_closed = None
        self.restart = None
        self.transport = None

        self.logger = logging.getLogger("server-udp")

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        self.logger.debug(f"Received data {data} from {addr}")
        data = construct_proper_json(data)
        for message in data:
            self.data_q.sync_q.put((addr, "UDP", message, None))

    def error_received(self, exc):
        self.restart_q.sync_q.put(True)

    def __call__(self):
        return self


class UDPClientProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        self.data_q = None

        self.is_closed = None
        self.transport = None

        self.logger = logging.getLogger("client-udp")

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        self.logger.debug(f"Received data {data} from server")
        data = construct_proper_json(data)
        for message in data:
            self.data_q.sync_q.put(("UDP", message, None))

    def __call__(self):
        return self
